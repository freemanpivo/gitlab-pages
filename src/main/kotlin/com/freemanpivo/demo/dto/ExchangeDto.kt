package com.freemanpivo.demo.dto

data class ExchangeDto (
        val base: String,
        val rates: HashMap<String, Long>
)