package com.freemanpivo.demo.api

import com.freemanpivo.demo.dto.ExchangeDto
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ExchangeController {

    @GetMapping("/")
    fun model(): ExchangeDto {
        return ExchangeDto("BRL", hashMapOf<String,Long>())
    }
}