package com.freemanpivo.demo.dto

data class ExchangeRateApiDto (
        val rates: HashMap<String, Long>,
        val base: String,
        val date: String
)